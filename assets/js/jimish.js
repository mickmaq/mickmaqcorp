/*-----------------------------------------------------------
 * Author           : Jimish Gajjar 
 * Version          : 1.0.0
 * File Description : Main js file of the template
 *------------------------------------------------------------
 */

// repeated variables
var $window = $(window);
var $root = $("html, body");

$(document).ready(function () {
    "use strict";

    menuToggler();
    $(".owl-item.active .hero-slide").addClass("zoom");
});

$(".to-contact").on("click", function () {
    $("section.active").removeClass("active");
    var $id = $(this).attr("href");
    $("#main").children($id).addClass("active");
});



/*-----------------------------------------------------------------------------
                                   FUNCTIONS
-----------------------------------------------------------------------------*/
/*-------------------------
    SUBMIT EMAIL
-------------------------*/
function submitToAPI(e){
    e.preventDefault();
var URL = "https://1t4yx7grhe.execute-api.us-west-2.amazonaws.com/prod/contact";
    var name = document.getElementById("name-input").value;
    var email = document.getElementById("email-input").value;
    var desc = document.getElementById("message-input").value;
    var msgElement = document.getElementById("returnMsg");

    
    msgElement.classList.remove("hiddennMsg");
    msgElement.classList.remove("errorMsg");
    msgElement.innerHTML = "Contacting...";
    msgElement.classList.remove("successMsg");

    if (name=="" || email=="" || desc=="")
     { 
        msgElement.classList.add("errorMsg");  
        msgElement.innerHTML = "Please Fill All Required Field";
         //alert("Please Fill All Required Field");
         return false;
     }
     
     //nameRE = /^[A-Z]{1}[a-z]{2,20}[ ]{1}[A-Z]{1}[a-z]{2,20}/;
	 nameRE=/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;
     if(!nameRE.test(name)) {
         //alert("Name entered, is not valid");
         msgElement.classList.add("errorMsg"); 
         msgElement.innerHTML = "Name entered, is not valid"; 
             return false;
     }
     
     
     emailRE = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
     if(!emailRE.test(email)) {
         //alert("Email Address entered, is not valid");
         msgElement.classList.add("errorMsg"); 
         msgElement.innerHTML = "Email Address entered, is not valid";  
             return false;
     }
    var data = {
       email : email,
       name : name,
       desc : desc
     };

     $.ajax({
      type: "POST",
      url : "https://1t4yx7grhe.execute-api.us-west-2.amazonaws.com/prod/contact",
      dataType: "json",
      crossDomain: "true",
      contentType: "application/json; charset=utf-8",
      data: JSON.stringify(data),


      success: function () {
        // clear form and show a success message
        msgElement.classList.add("successMsg");  
        msgElement.innerHTML = "Thank you for your interest.";  
        document.getElementById("contactForm").reset();
    //location.reload();
      },
      error: function () {
        // show an error message
        msgElement.innerHTML = "Error : Please contact site administrator";  
        msgElement.classList.add("errorMsg"); 
      }});
 

   
 } 

/*-------------------------
    MENU TOGGLER
-------------------------*/
function menuToggler() {
    "use strict";

    $(".overlay-menu-toggler").on("click", function () {
        $(".overlay-menu").addClass("show");
    });
    $(".overlay-menu").on("click", function () {
        $(this).removeClass("show");
    });
}